#include "header.h"

int main(int argc, char** argv)
{
  int i;

  if(argc==1){
      printf("\n\tUsage ./executable [number of particles] ...\n\n");
      return(-1);
    }

  const int number_particles = atoi(argv[1]);
  

  struct Str_result tmp;
  tmp = updater(number_particles,
  			10.0,
  			1.0,
  			5.0,
  			12.0,
  			0.001);
  
  for(i=0; i < number_particles; i++){
       printf("Number_of_collision value = %d \n",tmp.Number_of_collision[i]);
       printf("Energy value = %f \n",tmp.Efinal[i]);
  }
  
  return 0;
}

