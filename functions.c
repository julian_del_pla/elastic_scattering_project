#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "header.h"


/* This function is the core of the model */

struct Str_result updater(const int particle_number, /* number of particles            */
	    const float material_width,      /* with of the material           */
	    const double mean_path,         /* mean free path of the material */
	    const double Ei,                /* initial energy of the neutron  */
	    const double A,                 /* Mass number of the material    */
	    const double E_thr             /* Threshold energy               */
	    )             
{
  /* seed */
  srand((unsigned int)time(NULL));
  
  /* Particle array */
  struct Particle *P = (struct Particle *) malloc(particle_number *
						  sizeof(struct Particle));
  /* fix the initial energy of every neutron and the position */
  struct Str_result res = {NULL,NULL};
  int i;

  for (i=0 ; i<particle_number; i++)
      P[i].X  = displacement(RN, mean_path);

  /* array to store final energies of every particle */

  res.Number_of_collision = (int *) malloc( particle_number * sizeof(int));
  res.Efinal = (double *) malloc( particle_number * sizeof(double));  
 

  for (i=0 ; i<particle_number ; i++)
    {
      double alpha;
      double tmp_X = P[i].X;
      double tmp_E = Ei;
      int cou = 0;
      /* follow the path of every single neutron */
      while(tmp_X <= material_width && tmp_X >= 0.0)
	{
	  alpha = angle(RN);
	  tmp_X += cos(alpha) * displacement(RN, mean_path);
          tmp_E = final_energy(tmp_E, A, alpha);
	  if (tmp_E <= E_thr)
	    {
	      tmp_E = 0.0;
	      break;
	    }
          cou++;
	}
      
      res.Number_of_collision[i] = cou;
      res.Efinal[i] = Ei - tmp_E;
    }

  /* Deallocate memory */
  free(P);

  return res;
}


void dealloc( struct Str_result *result )
{
  free(result->Number_of_collision);
  free(result->Efinal);
}

inline double displacement(const double random_number, const double mean_path)
{
  return -log(random_number) * mean_path;
}

inline double angle(const double random_number)
{
  return acos(1.0 - 2.0 * random_number);
}
