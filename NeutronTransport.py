#!/usr/bin/python
import ctypes as C
import matplotlib.pyplot as plt
import sys
import numpy as np

clib = C.CDLL('./libNeutronScattering.so')

class Str_result( C.Structure ):
    _fields_=[
              ( "Number_of_collisions", C.POINTER(C.c_int ) ),
              ( "Efinal", C.POINTER(C.c_double) )
             ]
       

#input reader

def in_reader( file_obj ):
    ''' 
    Basic parser of the input files. Recognices hashtag # as special character to mark beggining of a comment.
    Also recognices units for density, energy and distance, multiplies for the appropiate factor to have final 
    value in g/cm3, eV and cm, wich are the internal units the algorithms use.  
    '''
    lines = file_obj.readlines( )
    for n, line in enumerate( lines ):
        if  line.strip( ).find( '#' ) != -1:
            line = line[ : line.strip( ).index( '#' ) ]

        if "Density" in line:
            density = float(line.split()[1])
            if len( line.split( ) ) == 3 and line.split( )[ 2 ] in density_unit_dict:
                density = density * density_unit_dict[line.split( )[ 2 ]]
        elif "Material" in line:
            material = line.split()[1]
        elif "N_Samples" in line:
            n_samples = int(float(line.split()[1]))
        elif "Energy_Cutoff" in line:
            e_cutoff = float(line.split()[1])
            if len( line.split( ) ) == 3 and line.split( )[ 2 ] in energy_unit_dict:
                e_cutoff = e_cutoff * energy_unit_dict[line.split( )[ 2 ]]
        elif "Energy" in line:
            e_initial = float(line.split()[1])
            if len( line.split( ) ) == 3 and  line.split( )[ 2 ] in energy_unit_dict:
                e_initial = e_initial * energy_unit_dict[line.split( )[ 2 ]]
        elif "Width" in line:
            width = float(line.split()[1])
            if len( line.split( ) ) == 3 and line.split( )[ 2 ] in distance_unit_dict:
                width = width * distance_unit_dict[line.split( )[ 2 ]]

    return ( material, density, width, n_samples, e_initial, e_cutoff ) 

#Dictionary of data.
'''
 Dictionary that contains the information necesary to compute the mean free path, 
 that is in ordered fashion, microscopic elastic scattering cross section and atomic mass number.  
 It's accesed by using the simbol of the pure material as key. Only 'C' (C-12) , 'Ni'(Ni-58) that  and 'O'(O-16)
 are defined so far. Values taken from https://www.ncnr.nist.gov/resources/n-lengths/.
'''
material_dict = {'C':(5.551,12.011),'Ni':(26.1,57.9353429), 'O':(4.232,15.994914)}

#Dictionary of physical units.
energy_unit_dict = {'MeV':1e+6,'meV':1e-3,'eV':1.0,'keV':1e+3,'GeV':1e+9}
distance_unit_dict = {'m':1e+2,'mm':1e-1,'cm':1.0,'km':1e+5}
density_unit_dict = {'g/cm3':1.0,'kg/m3':1e-3,'g/mL':1.0}

#Opening Input file 
input_file = open( sys.argv[1], 'r' )

#Reading data from file and parsing the necesary information.
material, density, width, n_samples, e_initial, e_cutoff = in_reader( input_file )

#Processing input data
micro_cross_sect, mass = material_dict[material]
mean_free_path = 1.0/(density*0.6022*micro_cross_sect/mass)

#Setting up the updater inputs and outputs types

clib.updater.restype =  Str_result
clib.updater.argtypes = [ C.c_int, C.c_float, C.c_double, C.c_double, C.c_double, C.c_double ]

BUNCHSIZE = 100000
N_BINS = 300
HIT_BINS = 60

tail=None

#Calling the updater in bunches of BUNCHSIZE
if n_samples > BUNCHSIZE:
    n_calls = n_samples/BUNCHSIZE
    if isinstance( n_calls, float ):
        n_calls = int(n_calls)
        tail = ( n_samples/BUNCHSIZE - n_calls ) * BUNCHSIZE
else:
    n_calls = 1
    BUNCHSIZE = n_samples


Hist_energy = np.zeros(N_BINS)
Hist_N_collisions = np.zeros( HIT_BINS, dtype = int )

for j in range(n_calls):
    if j == n_calls - 1 and tail != None:
        BUNCHSIZE = tail

    result = clib.updater( BUNCHSIZE, width, mean_free_path, e_initial, mass, e_cutoff)

    Energy_list = np.array( [ result.Efinal[i] for i in range( BUNCHSIZE ) if result.Efinal[i] != 0.0 ] )
    N_collision_list = np.array( [ result.Number_of_collisions[i] for i in range( BUNCHSIZE ) if result.Number_of_collisions[i] != 0.0 ] )
    
    
    clib.dealloc(C.byref(result))
    
    Hist_energy += np.histogram( Energy_list, bins = N_BINS )[0]
    Hist_N_collisions += np.histogram(N_collision_list, bins = HIT_BINS, range = (0,HIT_BINS))[0]


plt.axis([0,e_initial,0,np.amax(Hist_energy)])
plt.bar(np.linspace(0,e_initial,num=N_BINS),Hist_energy,0.02)
plt.savefig('Hist_energy.png')
#plt.show()

plt.axis([0,HIT_BINS,0,np.amax(Hist_N_collisions)])
plt.bar(np.linspace(0,HIT_BINS,num=HIT_BINS),Hist_N_collisions)
plt.savefig('Hist_N_collsion.png')
#plt.show()
