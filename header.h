#ifndef HEADER_H
#define HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/**
 * @file header.h
 * @author doxygen Sajad Azizi
 * @date 18 Mar 2016
 * 
 * @brief File containing example of doxygen usage for quick reference.
 *
 * In this header file we define a Struct that holds the instantaneus position for a given particle,
 * the Struct in wich we return the results to python, the updater function that updates the position and energy of a particle
 * @@.
 */


/* MACRO to get a random number in X in [0-1] */
/**
 *@param RN is a random number generator. 
*/
#define RN ((double)rand()/(RAND_MAX))

/* Particle structure definition */
struct Particle{
  double X;     /* X position */
};
struct Str_result{
 /**
   * @param Number_of_collision is pointer to an array of integers which are allocated and 
   * initialized in the updater function. Contains the number of collision that each particle 
   * in the array of Particles suffered.
 */
    int *Number_of_collision;
  /**
   * @param Efinal is a pointer to an array of doubles which are allocated and 
   * initialized in the updater function. Contains the final energy for each particle 
   * in the array of Particles.
 */   
    double *Efinal;
};


/* function prototypes */
struct Str_result  updater(const int, const float, const double, const double, const double, const double);
/**
 * This function takes a several arguments as input and returns a struct of type Str_result.
 * This function takes an array of particles and modifies the positions and energies by generating collision events, in each 
 * event updating the position of the particles, by calling the functions displacement and angle, and its energy by calling 
 * the final_energy function. 
 * @param particle_number this is an integer that indicates number of Particles to be processed 
 * @param material_width a float varitional that indicates the width of the material that the nesutron is bombarded upon
 * @param mean_path a double that fixes the mean free path for the neutron for a given material considering only the elastic
 * scattering of low energy neutrons
 * @param Ei a double that has the initial energy of the neutron
 * @param A a double that indicates the Mass number of the atoms in the material (only pure atomic materials implemented) 
 * @param E_thr a double that indicates the cutoff energy or Threshold energy, when a particles energy is lower than this 
 * value the calculation is finished for that particle.   
*/
double displacement(const double, const double);
/**
 * This function has 2 input arguments and returns a double that contains a random displacement,
 * taken from a distribution that has as a mean the mean free path. Used to change the position of the particles.
 * @param  random_number a random number between 0 and 1, of type double
 * @param mean_path a double that contains the mean free path 
 */

double angle(const double);
/**
 * This function have 1 input argument and returns a random angle taken from an empirical angular distribution.
 * @param  random_number this is a random number between 0 and 1
*/
double final_energy(const double, const double, const double);
/**
 * This function takes 3 arguments as input and returns the final energy of the particle after 
 * each collision event by solving the ecuations for a elastic collision where linear movement is conserved. 
 * @param Ei a double that contains the initial energy of the neutron
 * @param A a double that contains the Mass number of the material
 * @param thetaL a double varitional that contains the scattering angle in lab coordinate system
*/
void dealloc(struct Str_result *result);
/**
 * This function releases the dinamicaly allocated memory in the struct Str_result function and is 
 * necessary to free the memory from the python side of the code  
 * @param result a struct of type Str_result 
*/

#endif /* HEADER_H */
